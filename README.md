# descomplicando-gitlab

Treinamento Descomplicando o GitLab - Twitch

* Dia 1 - Entendendo o GitLab
	* O que é o Git
	* Entendemos o que é o GitLab
	* Como Criar um Grupo
	* Como Criar um Projeto
	* Aprendemos Comandos Básicos
	* Como Criar uma Branch
	* Como Criar uma Merge Request
	* Como adicionar membros no Projeto
	* Como fazer uma merge master/main

